// To parse this JSON data, do
//
//     final student = studentFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<products> productFromJson(String str) =>
    List<products>.from(json.decode(str).map((x) => products.fromJson(x)));

String productToJson(List<products> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class products {
  products({
    required this.pid,
    required this.name,
    required this.price,
    required this.description,
  });

  final pid;
  final name;
  final price;
  final description;

  factory products.fromJson(Map<String, dynamic> json) => products(
        pid: json["pid"],
        name: json["name"],
        price: json["price"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "pid": pid,
        "name": name,
        "price": price,
        "description": description,
      };
}
