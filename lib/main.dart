import 'package:crudmysql_api/update_product.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'product.dart';
import 'list_products.dart';
import 'insert_product.dart';
import 'about.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<products>> product;

  @override
  void initState() {
    super.initState();
    product = getProductList();
  }

  Future<List<products>> getProductList() async {
    String url = "http://10.0.2.2/flutter_api/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load students');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        backgroundColor: Colors.lightBlueAccent,
        child: ListView(
          padding: EdgeInsets.all(50),
          children: [
            ListTile(
                title: const Text('MENU'),
                leading: const Icon(Icons.list),
                onTap: () {
                  Navigator.of(context).pop();
                }),
            const Divider(),
            ListTile(
              title: const Text('About'),
              leading: const Icon(Icons.person),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return About();
                  // SystemNavigator.pop();
                }));
              },
            ),
            const Divider(),
            ListTile(
                title: const Text('Close'),
                leading: const Icon(Icons.close),
                onTap: () {
                  Navigator.of(context).pop();
                }),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Product List'),
        actions: [
          IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return About();
                  // SystemNavigator.pop();
                }));
              }),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<products>>(
          future: product,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    trailing: Icon(Icons.list),
                    title: Text(
                      data.name,
                      style: TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Details(product: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create()),
          );
        },
      ),
    );
  }
}
